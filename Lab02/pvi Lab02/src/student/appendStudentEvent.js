import createCheckbox from "../table/checkboxCreate.js";
import createDeleteButton from "../table/deleteButtonCreate.js";
import createStatusCircle from "../table/statusCircleCreate.js";
import editButtonCreate from "../table/editButtonCreate.js";
import clearInput from "../table/clearInput.js";

function openAddModalWindow() {
    const addButton = document.getElementById("add-button");
    addButton.addEventListener("click", () => {
        clearInput();
        const addModalOverlay = document.querySelector(".addModal-overlay");
        addModalOverlay.style.display = "block";
        const title = document.querySelector(".form-title");
        title.textContent = "Add student";
        const createButton = document.getElementById("create-modal");
        createButton.textContent = "Create";
    });
}

function createStudent(table) {
    const createStudentButton = document.getElementById("create-modal");
    createStudentButton.addEventListener("click", (event) => {
        const form = document.querySelector(".add-formContent");
        if (!form.checkValidity()) {
            return;
        }
        event.preventDefault();
        if (!createStudentButton.classList.contains("save-button")) {
            const row = table.insertRow();
            const statusCircle = createStatusCircle();
            const editButton = editButtonCreate();
            const deleteButton = createDeleteButton();
            const checkbox = createCheckbox();

            const cells = [];
            for (let i = 0; i < 7; i++) {
                const cell = document.createElement("td");
                cells.push(cell);
            }
            cells[0].appendChild(checkbox);
            const appendStudent = {
                group:document.getElementById("groupChoose").value,
                firstName:document.getElementById("firstName").value,
                lastName:document.getElementById("lastName").value,
                gender:document.getElementById("genderChoose").value,
                birthday:document.getElementById("birthday").value
            };
            const requestBody = JSON.stringify(appendStudent);
            cells[1].textContent = appendStudent.group;
            const firstName = appendStudent.firstName;
            const lastName = appendStudent.lastName;
            cells[2].textContent = `${firstName} ${lastName}`;
            cells[3].textContent = appendStudent.gender;
            cells[4].textContent = appendStudent.birthday;
            cells[5].appendChild(statusCircle);
            cells[6].appendChild(editButton);
            cells[6].appendChild(deleteButton);
            for (let cell of cells) {
                row.appendChild(cell);
            }
            clearInput();
            event.target.closest(".overlay").style.display = "none";
        }
    });
}

export default function appendStudentEvent() {
    openAddModalWindow();
    const table = document.querySelector("table");
    createStudent(table);
}