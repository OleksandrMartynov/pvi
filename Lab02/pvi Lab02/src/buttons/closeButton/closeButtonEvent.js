import clearInput from "../../table/clearInput.js";
export default function closeButtonEvent() {
    const closeButtons = document.getElementsByClassName("closeButton");
    for (let button of closeButtons) {
        button.addEventListener("click", (event) => {
            clearInput();
            const createButton = document.getElementById("create-modal");
            createButton.classList.remove("save-button");
            event.target.closest(".overlay").style.display = "none";
        });
    }
}