export default function profileButtonEvent(){
    const profileButton = document.querySelector("#profile-button");
    profileButton.addEventListener("click", () => {
        const profileInfo = document.querySelector(".profile-link");
        if (profileInfo.style.display) {
            profileInfo.style.display = "";
        } else {
            profileInfo.style.display = "none";
        }
    });
}
