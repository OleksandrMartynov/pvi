export default function createStatusCircle() {
    const statusCircle = document.createElement("div");
    statusCircle.style.width = "25px";
    statusCircle.style.height = "25px";
    statusCircle.style.borderRadius = "50%";
    statusCircle.style.backgroundColor = "grey";
    statusCircle.style.margin = "auto";
    return statusCircle;
}
