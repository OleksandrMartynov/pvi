import clearInput from "./clearInput.js";

const chooseOption = (select, value) => {
    for (let option of select.options) {
        if (option.value === value) {
            option.selected = true;
            return;
        }
    }
}

export default function editButtonEvent() {
    const createButton = document.getElementById("create-modal");
    const table = document.querySelector("table");
    let rowIndex;
    table.addEventListener("click", (event) => {
        if (event.target.closest("button").classList.contains("editButton")) {
            const addModalOverlay = document.querySelector(".addModal-overlay");
            addModalOverlay.style.display = "block";
            rowIndex = event.target.closest("tr").rowIndex;
            const title = document.querySelector(".form-title");
            title.textContent = "Edit student";
            createButton.textContent = "Save";
            const groupChoose = document.getElementById("groupChoose");
            chooseOption(groupChoose, table.rows[rowIndex].cells[1].textContent);
            const genderChoose = document.getElementById("genderChoose");
            chooseOption(genderChoose, table.rows[rowIndex].cells[3].textContent);
            const firstName = document.getElementById("firstName");
            firstName.value = table.rows[rowIndex].cells[2].textContent.split(" ")[0];
            const lastName = document.getElementById("lastName");
            lastName.value = table.rows[rowIndex].cells[2].textContent.split(" ")[1];
            const birthday = document.getElementById("birthday");
            birthday.value = table.rows[rowIndex].cells[4].textContent;
            createButton.classList.add("save-button");
        }
    });
    createButton.addEventListener("click", (event) => {
        const form = document.querySelector(".add-formContent");
        if (!form.checkValidity()) {
            return;
        }
        event.preventDefault();
        const table = document.querySelector("table");
        table.rows[rowIndex].cells[1].textContent = document.getElementById("groupChoose").value;
        const firstName = document.getElementById("firstName").value;
        const lastName = document.getElementById("lastName").value;
        table.rows[rowIndex].cells[2].textContent = `${firstName} ${lastName}`;
        table.rows[rowIndex].cells[3].textContent = document.getElementById("genderChoose").value;
        table.rows[rowIndex].cells[4].textContent = document.getElementById("birthday").value;
        createButton.classList.remove("save-button");
        clearInput();
        event.target.closest(".overlay").style.display = "none";
    })
}