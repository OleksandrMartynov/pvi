export default function editButtonCreate() {
    const editButton = document.createElement("button");
    editButton.style.margin = "auto";
    editButton.style.marginRight = "4px";
    editButton.style.backgroundColor = "white";

    const pencilImg = document.createElement("img");
    pencilImg.src = "/public/images/pencil.png";
    pencilImg.alt = "Edit";
    pencilImg.style.width = "12px";
    pencilImg.style.height = "12px";
    editButton.appendChild(pencilImg);
    editButton.classList.add("editButton");
    return editButton;
}