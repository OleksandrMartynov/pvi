<?php

require 'validator.php';
require 'service.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

if($_SERVER['REQUEST_METHOD'] === 'OPTIONS'){
 header('Access-Control-Allow-Origin: *');
 header('Access-Control-Allow-Methods: *');
 header('Access-Control-Allow-Headers: *');
}


if($_SERVER['REQUEST_METHOD'] === 'GET'){
	// $sql = $pdo->prepare("SELECT * FROM students");
	// $sql->execute();
	// $result = $sql->fetchAll(PDO::FETCH_OBJ);
  $result = getStudents();
  if(count($result)==0){
    $response = array('status' =>false,message=>"Connection db failed" );
    http_response_code(502);
    echo json_encode($response);
    exit();
  }
	echo json_encode($result);
}

if($_SERVER['REQUEST_METHOD'] === 'POST'){
	if(!verify()){
		exit();
	}
	$requestBody = file_get_contents('php://input');
    $student = json_decode($requestBody);
    $studentGroup = $student->group;
    $studentFirstName = $student->firstName;
    $studentLastName = $student->lastName;
    $studentGender = $student->gender;
    $studentBirthday = $student->birthday;
    createStudent($studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday);
    // $sql = ("INSERT INTO students (groupa,firstName,lastName,gender,birthday) VALUES (?,?,?,?,?)");
    // $query = $pdo->prepare($sql);
    // $query->execute([$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday]);
    http_response_code(201);
    $response = array("success"=>true);
    $response["data"]=$student;
    echo json_encode($response);	
}

if($_SERVER['REQUEST_METHOD'] === 'PUT'){
	if(!verify()){
		exit();
	}
	$requestBody = file_get_contents('php://input');
    $student = json_decode($requestBody);
    $studentGroup = $student->group;
    $studentFirstName = $student->firstName;
    $studentLastName = $student->lastName;
    $studentGender = $student->gender;
    $studentBirthday = $student->birthday;
    $studentID = $_GET['id'];
    updateStudent($studentID,$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday);
    // $sql = ("UPDATE students SET groupa=?, firstName=?,lastName=?,gender=?,birthday=? WHERE id=?");
    // $query = $pdo->prepare($sql);
    // $query->execute([$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday,$studentID]);
    http_response_code(200);
    $response = array("success"=>true);
    $response["data"]=$student;
    echo json_encode($response);	
}

if($_SERVER['REQUEST_METHOD'] === 'DELETE'){
	// $sql = ("DELETE FROM students WHERE id=?");
	// $query = $pdo->prepare($sql);
  $studentID = $_GET['id'];
  deleteStudent($studentID);
  // $query->execute([$studentID]);
  http_response_code(204);
  $response = array("success"=>true);
  echo json_encode($response);	
}