<?php

include 'db.php';

function getStudents(){
	global $pdo;
	$sql = $pdo->prepare("SELECT * FROM students");
	$sql->execute();
	$result = $sql->fetchAll(PDO::FETCH_OBJ);
	return $result;
}

function createStudent($studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday){
	global $pdo;
	$sql = ("INSERT INTO students (groupa,firstName,lastName,gender,birthday) VALUES (?,?,?,?,?)");
    $query = $pdo->prepare($sql);
    $query->execute([$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday]);
}

function updateStudent($studentID,$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday){
	global $pdo;
	$sql = ("UPDATE students SET groupa=?, firstName=?,lastName=?,gender=?,birthday=? WHERE id=?");
    $query = $pdo->prepare($sql);
    $query->execute([$studentGroup,$studentFirstName,$studentLastName,$studentGender,$studentBirthday,$studentID]);
}

function deleteStudent($studentID){
	global $pdo;
	$sql = ("DELETE FROM students WHERE id=?");
	$query = $pdo->prepare($sql);
	$query->execute([$studentID]);
}