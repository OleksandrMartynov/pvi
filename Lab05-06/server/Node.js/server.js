const http = require('http');
const express = require('express');
const {Server} = require("socket.io");
const mongoose = require('mongoose').default;
const dotenv = require("dotenv");
const Message = require("./src/message/model");
const Room = require("./src/room/model");
const messageRouter = require("./src/message/router");

dotenv.config();

const app = express();
app.use(messageRouter);
const server = http.createServer(app);
const io = new Server(server, {
    cors: {
        origin: "*",
    }
});

io.on('connection', async (socket) => {
    const rooms = await Room.find();
    socket.emit('rooms', rooms);
    socket.on('joinRoom', async (room, user) => {
        socket.join(room);
        const messages = await Message.find({room})
            .sort({timestamp: 1});
        const roomDB = await Room.findOne({
            name: {
                $eq: room
            }
        });
        const currentMembers = roomDB.members;
        if (!currentMembers.includes(user)) {
            currentMembers.push(user);
            await Room.findByIdAndUpdate(roomDB._id, {members: currentMembers});
        }
        socket.emit('messages', messages);
        socket.emit("viewMembers", currentMembers);
    });

    socket.on('createRoom', async (room) => {
        let createdRoom;
        if (room.type === "Private") {
            createdRoom = await Room.create({name: room.name, type: room.type, members: room.members});
        } else {
            createdRoom = await Room.create({name: room.name, type: room.type});
        }
        await createdRoom.save();
        io.emit('roomCreated', createdRoom);
    });

    socket.on("lastMessages", async (username) => {
        const rooms = await Room.find({members: username});
        const messages = [];
        for (let room of rooms) {
            const roomMessages = await Message.find({room: room.name}).sort({timestamp: -1})
                .limit(3);
            messages.push(...roomMessages);
        }
        const socketUser = socket.handshake.query.username;
        console.log(socketUser);
        if (socketUser === username) {
            socket.emit("showLastMessages",
                messages.sort((a, b) => a.timestamp < b.timestamp ? 1 : -1)
                    .slice(0, 3)
                    .reverse());
        }
    });

    socket.on('message', async (data) => {
        const message = await Message.create({
            username: data.username,
            message: data.message,
            room: data.room,
            timestamp: new Date()
        });
        io.to(data.room).emit('message', message);
        const room = await Room.findOne({name: data.room});
        io.sockets.sockets.forEach(socket => {
            const socketUser = socket.handshake.query.username;
            if (room.members.includes(socketUser) && socketUser !== data.username) {
                console.log("emit");
                socket.emit("lastMessages", socketUser);
            }
        });
    });
});

const start = async () => {
    await mongoose.connect(process.env.DB_URL);
    const PORT = process.env.PORT || 3000;
    server.listen(PORT, () => console.log(`Server started on ${PORT} port!!!`));
}

start();