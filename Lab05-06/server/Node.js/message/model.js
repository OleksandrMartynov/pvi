const mongoose = require("mongoose").default;

const MessageSchema = new mongoose.Schema({
    username: String,
    message: String,
    room: String,
    timestamp: Date
})

const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;