const mongoose = require("mongoose").default;

const RoomSchema = new mongoose.Schema({
    name: {
        type: String,
        unique: true
    },
    members: {
        type: [String]
    },
    type:String
});

const Room = mongoose.model('Room', RoomSchema);

module.exports = Room;