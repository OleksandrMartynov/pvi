export default function clearInput() {
    const groupChoose = document.getElementById("groupChoose");
    groupChoose.selectedIndex = 0;
    const genderChoose = document.getElementById("genderChoose");
    genderChoose.selectedIndex = 0;
    const firstName = document.getElementById("firstName");
    firstName.value = "";
    const lastName = document.getElementById("lastName");
    lastName.value = "";
    const birthday = document.getElementById("birthday");
    birthday.value = "";
    document.getElementById("studentID").value = "";
}