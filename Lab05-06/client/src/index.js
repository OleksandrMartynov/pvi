import appendTableButtons from "./table/appendTableButtons.js";
import profileButtonEvent from "./buttons/headerButton/profileButtonEvent.js";
import appendStudentEvent from "./student/appendStudentEvent.js";
import deleteStudentEvent from "./student/deleteStudentEvent.js";
import statusCircleChange from "./table/statusCircleStateEvent.js";
import notificationButtonEvent from "./buttons/headerButton/notificationButtonEvent.js";
import closeButtonEvent from "./buttons/closeButton/closeButtonEvent.js";
import editButtonEvent from "./table/editButtonEvent.js";
import createStatusCircle from "./table/statusCircleCreate.js";
import editButtonCreate from "./table/editButtonCreate.js";
import createDeleteButton from "./table/deleteButtonCreate.js";
import createCheckbox from "./table/checkboxCreate.js";


document.addEventListener("DOMContentLoaded", async () => {
    const userName = sessionStorage.getItem("user");
    let userNameHeader = document.querySelector(".username-header");
    userNameHeader.textContent = userName;
    const response = await fetch("http://localhost/student/");
    console.log(response);
    if (!response || !response.ok || response.status>=500) {
        console.log("DB ERROR");
        const errorText = "Server dont have access to db,please wait";
        const errorParagraph = document.createElement("p");
        const errorHeader = document.createElement("h3");
        errorHeader.textContent = "DB connection error!!!";
        errorParagraph.textContent = errorText;
        errorParagraph.style.color = "#F2B2B2";
        const errorDiv = document.createElement("div");
        errorDiv.classList.add("validationError");
        errorDiv.appendChild(errorHeader);
        errorDiv.appendChild(errorParagraph);
        document.body.appendChild(errorDiv);
        setTimeout(() => {
            errorDiv.remove();
        }, 8000);
        return;
    }
    const students = Array.from(await response.json());

    const table = document.querySelector("table");
    for (let student of students) {
        const row = table.insertRow();
        row.setAttribute("data-id", student.id);
        const statusCircle = createStatusCircle();
        const editButton = editButtonCreate();
        const deleteButton = createDeleteButton();
        const checkbox = createCheckbox();
        const cells = [];
        for (let i = 0; i < 7; i++) {
            const cell = document.createElement("td");
            cells.push(cell);
        }
        cells[0].appendChild(checkbox);
        cells[1].textContent = student.groupa;
        const firstName = student.firstName;
        const lastName = student.lastName;
        cells[2].textContent = `${firstName} ${lastName}`;
        cells[3].textContent = student.gender;
        cells[4].textContent = student.birthday;
        cells[5].appendChild(statusCircle);
        cells[6].appendChild(editButton);
        cells[6].appendChild(deleteButton);
        for (let cell of cells) {
            row.appendChild(cell);
        }
    }
});

appendTableButtons();
profileButtonEvent();
appendStudentEvent();
deleteStudentEvent();
statusCircleChange();
notificationButtonEvent();
closeButtonEvent();
editButtonEvent();