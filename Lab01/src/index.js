import appendTableButtons from "./table/appendTableButtons.js";
import profileButtonEvent from "./buttons/headerButton/profileButtonEvent.js";
import appendStudentEvent from "./student/appendStudentEvent.js";
import deleteStudentEvent from "./student/deleteStudentEvent.js";
import statusCircleChange from "./table/statusCircleStateEvent.js";
import notificationButtonEvent from "./buttons/headerButton/notificationButtonEvent.js";
import closeButtonEvent from "./buttons/closeButton/closeButtonEvent.js";

appendTableButtons();
profileButtonEvent();
appendStudentEvent();
deleteStudentEvent();
statusCircleChange();
notificationButtonEvent();
closeButtonEvent();
