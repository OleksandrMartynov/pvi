export default function closeButtonEvent() {
    const closeButtons = document.getElementsByClassName("closeButton");
    for (let button of closeButtons) {
        button.addEventListener("click", (event) => {
            event.target.closest(".overlay").style.display = "none";
        });
    }
}