export default function notificationButtonEvent() {
    const notificationButton = document.getElementById("notification-button");
    notificationButton.addEventListener("click", () => {
        const notificationCircle = document.querySelector(".notification-circle");
        notificationCircle.classList.add("new-notification");
    });

    notificationButton.addEventListener("click", (event) => {
        document.querySelector(".messageBoard").style.display = "block";
    });

    notificationButton.addEventListener("mouseout", () => {
        document.querySelector(".messageBoard").style.display="";
    });
}