import createCheckbox from "../table/checkboxCreate.js";
import createDeleteButton from "../table/deleteButtonCreate.js";
import createStatusCircle from "../table/statusCircleCreate.js";
import editButtonCreate from "../table/editButtonCreate.js";

function openAddModalWindow() {
    const addButton = document.getElementById("add-button");
    addButton.addEventListener("click", () => {
        const addModalOverlay = document.querySelector(".addModal-overlay");
        addModalOverlay.style.display = "block";
    });
}

function createStudent(table){
    const createStudentButton = document.getElementById("create-modal");
    createStudentButton.addEventListener("click", (event) => {
        const row = table.insertRow();
        const statusCircle = createStatusCircle();
        const editButton = editButtonCreate();
        const deleteButton = createDeleteButton();
        const checkbox = createCheckbox();

        const cells = [];
        for (let i = 0; i < 7; i++) {
            const cell = document.createElement("td");
            cells.push(cell);
        }
        cells[0].appendChild(checkbox);
        cells[1].textContent = "KN-21";
        cells[2].textContent = "Taras Shevchenko";
        cells[3].textContent = "M";
        cells[4].textContent = "09.03.1814";
        cells[5].appendChild(statusCircle);
        cells[6].appendChild(editButton);
        cells[6].appendChild(deleteButton);
        for (let cell of cells) {
            row.appendChild(cell);
        }
        event.target.closest(".overlay").style.display = "none";
    });
}

export default function appendStudentEvent() {
    openAddModalWindow();
    const table = document.querySelector("table");
    createStudent(table);
}