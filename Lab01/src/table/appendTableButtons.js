import createStatusCircle from "./statusCircleCreate.js";
import editButtonCreate from "./editButtonCreate.js";
import createDeleteButton from "./deleteButtonCreate.js";
import createCheckbox from "./checkboxCreate.js";

export default function appendTableButtons(){
    const table = document.getElementsByTagName("table")[0];
    for (let i = 1; i < table.rows.length; i++) {
        const statusCircle = createStatusCircle();
        table.rows[i].cells[5].appendChild(statusCircle);

        const editButton = editButtonCreate();
        table.rows[i].cells[6].appendChild(editButton);

        const deleteButton = createDeleteButton();
        table.rows[i].cells[6].appendChild(deleteButton);

        const checkbox = createCheckbox();
        table.rows[i].cells[0].appendChild(checkbox);
    }
}