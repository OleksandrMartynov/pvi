import appendTableButtons from "./table/appendTableButtons.js";
import profileButtonEvent from "./buttons/headerButton/profileButtonEvent.js";
import appendStudentEvent from "./student/appendStudentEvent.js";
import deleteStudentEvent from "./student/deleteStudentEvent.js";
import statusCircleChange from "./table/statusCircleStateEvent.js";
import notificationButtonEvent from "./buttons/headerButton/notificationButtonEvent.js";
import closeButtonEvent from "./buttons/closeButton/closeButtonEvent.js";
import editButtonEvent from "./table/editButtonEvent.js";

//1. Form validation
//2. Edit fields

appendTableButtons();
profileButtonEvent();
appendStudentEvent();
deleteStudentEvent();
statusCircleChange();
notificationButtonEvent();
closeButtonEvent();
editButtonEvent();