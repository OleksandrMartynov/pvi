const CACHE_NAME = 'my-pwa-cache';
const DIR_NAME = "pvi Lab02/";
const urlsToCache = [
    'index.html',
    'styles.css',
    'sw.js',
    'manifest.json',
    'public/images/bell.png',
    'public/images/pencil.png',
    'public/images/user.png',
    'public/images/icon.png',
    'public/images/icon-192x192.png',
    'public/images/icon-512x512.png',
    'src/index.js',
    'src/buttons/closeButton/closeButtonEvent.js',
    'src/buttons/headerButton/notificationButtonEvent.js',
    'src/buttons/headerButton/profileButtonEvent.js',
    'src/student/appendStudentEvent.js',
    'src/student/deleteStudentEvent.js',
    'src/table/appendTableButtons.js',
    'src/table/checkboxCreate.js',
    'src/table/deleteButtonCreate.js',
    'src/table/editButtonCreate.js',
    'src/table/editButtonEvent.js',
    'src/table/statusCircleCreate.js',
    'src/table/statusCircleStateEvent.js',
    'src/table/clearInput.js'
];

for(let i=0;i<urlsToCache.length;i++){
    urlsToCache[i]=DIR_NAME + urlsToCache[i];
}

self.addEventListener('install', function (event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(async function (cache) {
                console.log('Opened cache');
                return await cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames.filter((cacheName) => {
                    return cacheName.startsWith('my-pwa-cache') &&
                        cacheName !== 'my-pwa-cache-v1';
                }).map(async (cacheName) => {
                    return await caches.delete(cacheName);
                })
            );
        })
    );
});


self.addEventListener('fetch', function (event) {
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                if (response) {
                    return response;
                }
                const fetchRequest = event.request.clone();
                return fetch(fetchRequest).then(
                    function (response) {
                        if (!response || response.status >= 300 || response.type !== 'basic') {
                            return response;
                        }
                        const responseToCache = response.clone();
                        caches.open(CACHE_NAME)
                            .then(async function (cache) {
                                await cache.put(event.request, responseToCache);
                            });
                        return response;
                    }
                );
            })
    );
});
