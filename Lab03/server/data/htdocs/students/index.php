<?php

header('Access-Control-Allow-Origin: *');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $allowed_groups = array("PZ-21", "PZ-22", "PZ-23", "PZ-24", "PZ-25", "PZ-26");

  $requestBody = file_get_contents('php://input');
  $student = json_decode($requestBody);
  $missing_fields = array();
  $invalid_fields = array();

  if (!isset($student->group)) {
    $missing_fields[] = "group";
  } elseif (!(is_string($student->group) && in_array($student->group, $allowed_groups))) {
    $invalid_fields[] = "group";
  }

  if (!isset($student->gender)) {
    $missing_fields[] = "gender";
  } elseif (!is_string($student->gender) || ($student->gender != "F" && $student->gender != "M")) {
    $invalid_fields[] = "gender";
  }

  if (!isset($student->birthday)) {
    $missing_fields[] = "birthday";
  } elseif (!is_string($student->birthday)) {
    $invalid_fields[] = "birthday";
  } elseif (strtotime($student->birthday) > time()) {
	 $invalid_fields[] = "birthday";
  }

  if (count($missing_fields) == 0 && count($invalid_fields) == 0) {
    http_response_code(201);
    $response = array("success"=>true);
    $response["data"]=$student;
  } else {
    http_response_code(400);
    $response = array("success" => false);
  
    if (count($missing_fields) > 0) {
      $response["missing_fields"] = $missing_fields;
    }
  
    if (count($invalid_fields) > 0) {
      $response["invalid_fields"] = $invalid_fields;
    }
  }
  echo json_encode($response);
} 