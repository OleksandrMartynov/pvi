const profileButton = document.querySelector("#profile-button");
profileButton.addEventListener("click", () => {
    const profileInfo = document.querySelector(".profile-link");
    if (profileInfo.style.display) {
        profileInfo.style.display = "";
    } else {
        profileInfo.style.display = "none";
    }
});

const loginLink = document.querySelector("#login");
loginLink.addEventListener("click", () => {
    const addModalOverlay = document.querySelector(".addModal-overlay");
    addModalOverlay.style.display = "block";
});

const loginButton = document.getElementById("loginModal");
loginButton.addEventListener("click", async (event) => {
    const form = document.querySelector(".add-formContent");
    if (!form.checkValidity()) {
        return;
    }
    event.preventDefault();
    const student = {
        userName: document.getElementById("userName").value,
        password: document.getElementById("password").value
    }
    const requestBody = JSON.stringify(student);
    const response = await fetch("http://localhost/login/", {
        method: "POST",
        body: requestBody,
        mode: "cors",
        headers: {
            'Access-Control-Request-Headers': 'origin,x-requested-with',
        },
    });
    if (!response.ok) {
        alert("Invalid login or password");
    } else {
        localStorage.setItem("user",student.userName);
        window.location.href = "http://localhost/App/fullPage.html";
    }
});