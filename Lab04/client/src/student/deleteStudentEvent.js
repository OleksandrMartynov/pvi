export default function deleteStudentEvent() {
    const table = document.querySelector("table");
    table.addEventListener("click", (event) => {
        if (event.target.classList.contains("deleteButton")) {
            const deleteModalOverlay = document.querySelector(".deleteWarning-overlay");
            deleteModalOverlay.style.display = "block";
            const deleteModalForm = document.querySelector(".delete-warning");
            const rowIndex = event.target.closest("tr").rowIndex;
            const deleteStudentName = table.rows[rowIndex].cells[2].textContent;
            const defaultModalMessage = deleteModalForm.querySelector("p").textContent;
            deleteModalForm.querySelector("p").textContent += ` ${deleteStudentName}`;

            const deleteButton = document.getElementById("deleteModal-button");
            deleteButton.addEventListener("click", async () => {
                const id = event.target.closest("tr").dataset.id;
                const response = await fetch(`http://localhost/student?id=${id}`, {
                    method: "DELETE",
                });
                if (response.status === 204) {
                    event.target.closest("tr").remove();
                    deleteModalOverlay.style.display = "none";
                    deleteModalForm.querySelector("p").textContent = defaultModalMessage;
                }
            });
        }
    });
}