export default function statusCircleChange() {
    const table = document.querySelector("table");
    table.addEventListener("click", (event) => {
        if (event.target.tagName === "INPUT" && event.target.type === "checkbox") {
            const row = event.target.closest("tr").rowIndex;
            if (event.target.checked) {
                console.log(table.rows[row].cells[5]);
                table.rows[row].cells[5].children[0].style.backgroundColor = "green";
            } else {
                table.rows[row].cells[5].children[0].style.backgroundColor = "grey";
            }
        }
    });
}