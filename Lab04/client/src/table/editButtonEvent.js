import clearInput from "./clearInput.js";

const chooseOption = (select, value) => {
    for (let option of select.options) {
        if (option.value === value) {
            option.selected = true;
            return;
        }
    }
}

export default function editButtonEvent() {
    const createButton = document.getElementById("create-modal");
    const table = document.querySelector("table");
    let rowIndex;
    table.addEventListener("click", (event) => {
        if (event.target.closest("button").classList.contains("editButton")) {
            const addModalOverlay = document.querySelector(".addModal-overlay");
            addModalOverlay.style.display = "block";
            rowIndex = event.target.closest("tr").rowIndex;
            const title = document.querySelector(".form-title");
            title.textContent = "Edit student";
            const editButton = event.target.closest("button");
            document.getElementById("studentID").value = editButton.closest("tr").dataset.id;
            createButton.textContent = "Save";
            const groupChoose = document.getElementById("groupChoose");
            chooseOption(groupChoose, table.rows[rowIndex].cells[1].textContent);
            const genderChoose = document.getElementById("genderChoose");
            chooseOption(genderChoose, table.rows[rowIndex].cells[3].textContent);
            const firstName = document.getElementById("firstName");
            firstName.value = table.rows[rowIndex].cells[2].textContent.split(" ")[0];
            const lastName = document.getElementById("lastName");
            lastName.value = table.rows[rowIndex].cells[2].textContent.split(" ")[1];
            const birthday = document.getElementById("birthday");
            birthday.value = table.rows[rowIndex].cells[4].textContent;
            createButton.classList.add("save-button");
        }
    });
    createButton.addEventListener("click", async (event) => {
        if (event.target.classList.contains("save-button")) {
            const form = document.querySelector(".add-formContent");
            if (!form.checkValidity()) {
                return;
            }
            event.preventDefault();
            const table = document.querySelector("table");
            const updatedStudent = {
                group: document.getElementById("groupChoose").value,
                firstName: document.getElementById("firstName").value,
                lastName: document.getElementById("lastName").value,
                gender: document.getElementById("genderChoose").value,
                birthday: document.getElementById("birthday").value
            };
            const requestBody = JSON.stringify(updatedStudent);
            const id = document.getElementById("studentID").value
            const response = await fetch(`http://localhost/student?id=${id}`, {
                method: "PUT",
                // headers: {
                //     'Access-Control-Request-Headers': 'origin,x-requested-with',
                // },
                body: requestBody,
                mode: "cors"
            });
            const responseBody = await response.json();
            if (responseBody.success === true) {
                table.rows[rowIndex].cells[1].textContent = document.getElementById("groupChoose").value;
                const firstName = document.getElementById("firstName").value;
                const lastName = document.getElementById("lastName").value;
                table.rows[rowIndex].cells[2].textContent = `${firstName} ${lastName}`;
                table.rows[rowIndex].cells[3].textContent = document.getElementById("genderChoose").value;
                table.rows[rowIndex].cells[4].textContent = document.getElementById("birthday").value;
                createButton.classList.remove("save-button");
                clearInput();
                event.target.closest(".overlay").style.display = "none";
            } else {
                const errorText = "You have invalid fields: " + responseBody.invalid_fields;
                const errorParagraph = document.createElement("p");
                const errorHeader = document.createElement("h3");
                errorHeader.textContent = "Server validation error!!!";
                errorParagraph.textContent = errorText;
                errorParagraph.style.color = "#F2B2B2";
                const errorDiv = document.createElement("div");
                errorDiv.classList.add("validationError");
                errorDiv.appendChild(errorHeader);
                errorDiv.appendChild(errorParagraph);
                document.body.appendChild(errorDiv);
                setTimeout(() => {
                    errorDiv.remove();
                }, 8000);
            }
        }
    })
}