export default function createDeleteButton() {
    const deleteButton = document.createElement("button");
    deleteButton.textContent = "X";
    deleteButton.style.margin = "auto";
    deleteButton.classList.add("deleteButton");
    deleteButton.style.backgroundColor = "white";
    return deleteButton;
}