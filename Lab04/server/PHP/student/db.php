<?php

 $host = 'localhost';
 $db = 'db';
 $user = 'root';
 $password = '';

 try {
 	$pdo = new PDO("mysql:host=$host;dbname=$db",$user,$password);
 } catch (PDOException $e) {
 	 http_response_code(502);
    exit();
 }
?>