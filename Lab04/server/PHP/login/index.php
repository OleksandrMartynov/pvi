<?php

require 'service.php';

header('Access-Control-Allow-Origin: *');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$requestBody = file_get_contents('php://input');
    $user = json_decode($requestBody);
    if(login($user)){
        http_response_code(200);
        exit();
    }
 	http_response_code(401);       
}